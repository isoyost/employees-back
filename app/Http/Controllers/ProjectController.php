<?php

namespace App\Http\Controllers;

/**
 * Class ProjectController
 *
 * @package App\Http\Controllers
 */
final class ProjectController extends CrudController
{
    public function __construct()
    {
        parent::__construct(\App\Models\Project::class);
    }
}
