<?php

namespace App\Http\Controllers;

/**
 * Class JobController
 *
 * @package App\Http\Controllers
 */
final class JobController extends CrudController
{
    public function __construct()
    {
        parent::__construct(\App\Models\Job::class);
    }
}
