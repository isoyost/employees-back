<?php

namespace App\Http\Controllers;

/**
 * Class EmployeeController
 *
 * @package App\Http\Controllers
 */
final class EmployeeController extends CrudController
{
    public function __construct()
    {
        parent::__construct(\App\Models\Employee::class);
    }
}
