<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class CrudController
 *
 * @package App\Http\Controllers
 */
abstract class CrudController extends Controller
{
    private string $modelClass;

    /**
     * CrudController constructor.
     *
     * @param string $modelClass
     */
    public function __construct(string $modelClass)
    {
        $this->modelClass = $modelClass;
    }

    /**
     * Retrieve all resources.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json($this->modelClass::all());
    }

    /**
     * Store newly created resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        return response()->json(
            $this->modelClass::create($request->all()),
            Response::HTTP_CREATED
        );
    }

    /**
     * Retrieve the resource for the given ID.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return response()->json($this->modelClass::findOrFail($id));
    }

    /**
     * Update the resource with the given ID.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(int $id, Request $request): Response
    {
        $this->modelClass::findOrFail($id)->update($request->all());

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Destroy the resource with the given ID.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id): Response
    {
        $this->modelClass::whereId($id)->delete();

        return response('', Response::HTTP_NO_CONTENT);
    }
}
