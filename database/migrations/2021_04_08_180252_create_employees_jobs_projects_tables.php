<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateEmployeesJobsProjectsTables
 */
final class CreateEmployeesJobsProjectsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('employees', function (Blueprint $table): void {
            $table->id();
            $table->string('name', 50);
            $table->string('email', 70)->unique();
            $table->decimal('salary', 8, 2, true);
            $table->timestamps();
        });

        Schema::create('jobs', function (Blueprint $table): void {
            $table->id();
            $table->string('title', 50)->unique();
            $table->decimal('minimal_salary', 8, 2, true);
            $table->timestamps();
        });

        Schema::create('projects', function (Blueprint $table): void {
            $table->id();
            $table->string('title', 50)->unique();
            $table->decimal('value', 11, 2, true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('projects');
        Schema::drop('jobs');
        Schema::drop('employees');
    }
}
