<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

/**
 * Class EmployeeSeeder
 *
 * @package Database\Seeders
 */
final class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Employee::factory()
            ->count(10)
            ->create();
    }
}
