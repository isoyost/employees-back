<?php

namespace Database\Seeders;

use App\Models\Job;
use Illuminate\Database\Seeder;

/**
 * Class JobSeeder
 *
 * @package Database\Seeders
 */
final class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Job::factory()
            ->count(10)
            ->create();
    }
}
