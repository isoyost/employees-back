<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Seeder;

/**
 * Class ProjectSeeder
 *
 * @package Database\Seeders
 */
final class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Project::factory()
            ->count(10)
            ->create();
    }
}
