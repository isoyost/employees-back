<?php

/**
 * @var \Laravel\Lumen\Routing\Router $router
 */

$resources = [
    'employees' => 'EmployeeController',
    'jobs' => 'JobController',
    'projects' => 'ProjectController',
];

$router->group([
    'where' => ['id' => '[0-9]+'],
    'prefix' => 'api',
], function () use ($resources, $router): void {
    foreach ($resources as $resource => $controller) {
        $router->group(['prefix' => $resource], function () use ($router, $controller): void {
            $router->get('', "{$controller}@index");
            $router->post('', "{$controller}@store");
            $router->get('{id}', "{$controller}@show");
            $router->put('{id}', "{$controller}@update");
            $router->delete('{id}', "{$controller}@destroy");
        });
    }
});
